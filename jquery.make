; jQuery Libraries

api = 2
core = 7.x


; Modules

projects[libraries] = 2


; Libraries

libraries[jquery][download][type] = file
libraries[jquery][download][url] = http://code.jquery.com/jquery.min.js
libraries[jquery][download][filename] = jquery.min.js
