# jQuery Libraries

The jQuery module allows to use the Libraries API module and update to later versions.

For a full description of the module, visit the project page:
http://drupal.org/project/jquery

To submit bug reports and feature suggestions, or to track changes:
http://drupal.org/project/issues/jquery


## Requirements

* Libraries API module


## Installation

Install as usual, see http://drupal.org/node/70151 for further information.


## Configuration

* @todo


## Usage

* @todo


## Contributors

* Daniel F. Kudwien (sun) - http://drupal.org/user/54136
* Rob Loach (RobLoach) - http://drupal.org/u/robloach
